﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace UMRT.Includes.Classes
{
    public class lazpizFNC
    {
        public string TPL_NAME = ConfigurationSettings.AppSettings.Get("tplName");
        public string APPS_NAME = ConfigurationSettings.AppSettings.Get("appsName");

        public class ChkUserLoginVal
        {
            public bool returnVal1 { get; set; }
            public string returnVal2 { get; set; }
            public string errorMsg { get; set; }
        }

        public class ChkChangePassVal
        {
            public bool actOper { get; set; }
            public string respMsg { get; set; }
        }

        public class CookiesManVal
        {
            public bool actOper { get; set; }
            public string respMsg { get; set; }
        }

        public static bool ChkUser()
        {
            bool result = true;
            try
            {
                HttpCookie reqCookies = HttpContext.Current.Request.Cookies["umrt_login"];
                if (reqCookies != null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception exp)
            {
                result = false;
            }
            return result;
        }

        public string retTopMenu(string pageFile)
        {
            string retVal = "";
            retVal += "<ul class=\"art-menu\">";
            if (true)
            {
                retVal += "<li><a href=\"/" + APPS_NAME + "/Default.aspx\" " + this.chkActiveTopMenu("Default.aspx", pageFile) + "><span class=\"l\"> </span><span class=\"r\"> </span><span class=\"t\">หน้าหลัก</span></a></li>";
                //retVal += "<li><a href=\"/" + APPS_NAME + "/Ordering.aspx\" " + this.chkActiveTopMenu("Ordering.aspx", pageFile) + "><span class=\"l\"> </span><span class=\"r\"> </span><span class=\"t\">รายการที่กำลังสั่งซื้อ</span></a></li>";
                //retVal += "<li><a href=\"/" + APPS_NAME + "/Ordered.aspx\" " + this.chkActiveTopMenu("Ordered.aspx", pageFile) + "><span class=\"l\"> </span><span class=\"r\"> </span><span class=\"t\">รายการที่สั่งซื้อไปแล้ว</span></a></li>";
                //retVal += "<li><a href=\"/" + APPS_NAME + "/SearchChem.aspx\" " + this.chkActiveTopMenu("SearchChem.aspx", pageFile) + "><span class=\"l\"> </span><span class=\"r\"> </span><span class=\"t\">ค้นหารายชื่อสารเคมี</span></a></li>";
                //retVal += "<li><a href=\"/" + APPS_NAME + "/SearchChemFCNum.aspx\" " + this.chkActiveTopMenu("SearchChemFCNum.aspx", pageFile) + "><span class=\"l\"> </span><span class=\"r\"> </span><span class=\"t\">ค้นหารายชื่อสารเคมี จาก C-Number</span></a></li>";
                //retVal += "<li><a href=\"/" + APPS_NAME + "/MatAdd.aspx\" " + this.chkActiveTopMenu("MatAdd.aspx", pageFile) + "><span class=\"l\"> </span><span class=\"r\"> </span><span class=\"t\">เพิ่มสารเคมี / วัสดุชนิดใหม่</span></a></li>";
                //retVal += "<li><a href=\"/" + APPS_NAME + "/ChPwd.aspx\" " + this.chkActiveTopMenu("ChPwd.aspx", pageFile) + "><span class=\"l\"> </span><span class=\"r\"> </span><span class=\"t\">เปลี่ยนแปลง Password</span></a></li>";
                //retVal += "<li><a href=\"/" + APPS_NAME + "/Logout.aspx\" " + this.chkActiveTopMenu("Logout.aspx", pageFile) + "><span class=\"l\"> </span><span class=\"r\"> </span><span class=\"t\">ออกจากระบบ</span></a></li>";
            }
            retVal += "</ul>";
            return (retVal);
        }

        public string retFooterMenu()
        {
            string retVal = "";
            if (true)
            {
                retVal += "<a href=\"/" + APPS_NAME + "/Default.aspx\">หน้าหลัก</a> | ";
                //retVal += "<a href=\"/" + APPS_NAME + "/Ordering.aspx\">รายการที่กำลังสั่งซื้อ</a> | ";
                //retVal += "<a href=\"/" + APPS_NAME + "/Ordered.aspx\">รายการที่สั่งซื้อไปแล้วกำลังสั่งซื้อ</a> | ";
                //retVal += "<a href=\"/" + APPS_NAME + "/SearchChem.aspx\">ค้นหารายชื่อสารเคมี</a> | ";
                //retVal += "<a href=\"/" + APPS_NAME + "/SearchChemFCNum.aspx\">ค้นหารายชื่อสารเคมี จาก C-Number</a> | ";
                //retVal += "<a href=\"/" + APPS_NAME + "/MatAdd.aspx\">เพิ่มสารเคมี / วัสดุชนิดใหม่</a> | ";
                //retVal += "<a href=\"/" + APPS_NAME + "/ChPwd.aspx\">เปลี่ยนแปลง Password</a> | ";
            };
            return (retVal);
        }

        private string chkActiveTopMenu(string thisFile,string curFile)
        {
            string retVal = "";
            string actTxt = "class=\"active\"";
            if (thisFile.ToLower() == curFile.ToLower())
            {
                retVal = actTxt;
            }
            else
            {
                retVal = "";
            }
            return (retVal);
        }

        public void DestroyCookie()
        {
            if (HttpContext.Current.Request.Cookies["lcsp_login"] != null)
            {
                HttpCookie myCookie = new HttpCookie("lcsp_login");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                HttpContext.Current.Response.Cookies.Add(myCookie);
            }
        }

    }
}