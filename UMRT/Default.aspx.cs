﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMRT.Includes.Classes;
using System.Globalization;

namespace UMRT
{
    public partial class Default : System.Web.UI.Page
    {
        protected lazpizFNC objFNC = new lazpizFNC();

        decimal sumFooterValue = 0;
        decimal sumQFooterValue = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.loadDDDate();
                this.loadDDMG();
                this.loadLogsGrid(null,null,null);
            }
        }

        private void loadDDDate()
        {
            DateTime now = DateTime.Now;
            string dayNowDate = now.Day.ToString();
            string monthNowDate = now.Month.ToString("d2");
            string yearNowDate = now.Year.ToString();
            ddStartDay.DataSource = DayArr();
            ddStartDay.DataTextField = "Value";
            ddStartDay.DataValueField = "Key";
            ddStartDay.SelectedValue = dayNowDate;
            ddStartDay.DataBind();
            ddStartMonth.DataSource = MonthArr();
            ddStartMonth.DataTextField = "Value";
            ddStartMonth.DataValueField = "Key";
            ddStartMonth.SelectedValue = monthNowDate;
            ddStartMonth.DataBind();
            ddStartYear.DataSource = YearArr();
            ddStartYear.DataTextField = "Value";
            ddStartYear.DataValueField = "Key";
            ddStartYear.SelectedValue = yearNowDate;
            ddStartYear.DataBind();

            ddEndDay.DataSource = DayArr();
            ddEndDay.DataTextField = "Value";
            ddEndDay.DataValueField = "Key";
            ddEndDay.SelectedValue = dayNowDate;
            ddEndDay.DataBind();
            ddEndMonth.DataSource = MonthArr();
            ddEndMonth.DataTextField = "Value";
            ddEndMonth.DataValueField = "Key";
            ddEndMonth.SelectedValue = monthNowDate;
            ddEndMonth.DataBind();
            ddEndYear.DataSource = YearArr();
            ddEndYear.DataTextField = "Value";
            ddEndYear.DataValueField = "Key";
            ddEndYear.SelectedValue = yearNowDate;
            ddEndYear.DataBind();
        }

        private void loadDDMG()
        {
            ddMatGrp.DataSource = MatGrpArr();
            ddMatGrp.DataTextField = "Value";
            ddMatGrp.DataValueField = "Key";
            //ddMatGrp.SelectedValue = dayNowDate;
            ddMatGrp.DataBind();
        }

        protected Dictionary<string, string> MatGrpArr()
        {
            Dictionary<string, string> dataMatGrpArr = new Dictionary<string, string>();
            try
            {
                dbUMRTDataContext dbConn = new dbUMRTDataContext();
                var getMatGrp = from g in dbConn.tbMatGrps
                                where g.Enable.Equals(true)
                                orderby g.matGrp ascending
                                select new
                                {
                                    mgID = g.ID.ToString(),
                                    mgName = g.matGrp
                                };
                if (getMatGrp.Count() > 0)
                {
                    dataMatGrpArr.Add("ALL", "ALL");
                    foreach (var i in getMatGrp)
                    {
                        dataMatGrpArr.Add(i.mgID, i.mgName);
                    }
                    return (dataMatGrpArr);
                }
                else
                {
                    dataMatGrpArr.Add("", "Data does't exists.");
                    return (dataMatGrpArr);
                }
            }
            catch (Exception exp)
            {
                dataMatGrpArr.Add("", exp.Message);
                return (dataMatGrpArr);
            }
        }

        protected Dictionary<string, string> DayArr()
        {
            //ArrayList dataMachArr = new ArrayList();
            Dictionary<string, string> dataDayArr = new Dictionary<string, string>();
            try
            {
                int ic;
                for (ic = 1; ic <= 31; ic++)
                {
                    dataDayArr.Add(ic.ToString(), ic.ToString());
                }
                return (dataDayArr);
            }
            catch (Exception exp)
            {
                dataDayArr.Add("item 1", "Item 1");
                return (dataDayArr);
            }
        }

        protected Dictionary<string, string> MonthArr()
        {
            Dictionary<string, string> dataMonthArr = new Dictionary<string, string>();
            try
            {
                int ic;
                string mStr;
                for (ic = 1; ic <= 12; ic++)
                {
                    mStr = (ic < 10) ? "0" + ic.ToString() : ic.ToString();
                    dataMonthArr.Add(mStr, mStr);
                }
                return (dataMonthArr);
            }
            catch (Exception exp)
            {
                dataMonthArr.Add("item 1", "Item 1");
                return (dataMonthArr);
            }
        }

        protected Dictionary<string, string> YearArr()
        {
            Dictionary<string, string> dataYearArr = new Dictionary<string, string>();
            try
            {
                System.DateTime nowDate = DateTime.Now;
                int thisYear = nowDate.Year + 1;
                DateTime lastYearVal = DateTime.Today.AddYears(-1);
                int lastYear = lastYearVal.Year;
                if (thisYear > 0)
                {
                    for (int year = lastYear; year <= thisYear; year++)
                    {
                        dataYearArr.Add(year.ToString(), year.ToString());
                    }
                    return (dataYearArr);
                }
                else
                {
                    dataYearArr.Add(thisYear.ToString(), thisYear.ToString());
                    return (dataYearArr);
                }
            }
            catch (Exception exp)
            {
                dataYearArr.Add("ALL", "All");
                return (dataYearArr);
            }
        }

        private void loadLogsGrid(string startDate, string endDate, string matGroup)
        {
            DateTime srcStartDT;
            DateTime srcEndDT;
            string matGroupSrc = "";
            if (string.IsNullOrEmpty(startDate) & string.IsNullOrEmpty(endDate))
            {
                DateTime srcDT = DateTime.Now;
                srcStartDT = srcDT;
                srcEndDT = srcDT;
            }
            else
            {
                srcStartDT = DateTime.ParseExact(startDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                srcEndDT = DateTime.ParseExact(endDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
            matGroupSrc = (matGroup == "ALL" || string.IsNullOrEmpty(matGroup)) ? "" : matGroup;
            //Response.Write(srcStartDT.ToShortDateString() + "<br/>");
            //Response.Write(srcEndDT.ToShortDateString() + "<br/>");
            //Response.Write(matGroupSrc + "<br/>");
            try
            {
                dbUMRTDataContext dbConn = new dbUMRTDataContext();
                var getBrand = (string.IsNullOrEmpty(matGroupSrc)) ? from b in dbConn.vwMatTrLogs where b.DelStat.Equals(false) & b.MatRecvDate.Value.Date >= srcStartDT.Date & b.MatRecvDate.Value.Date <= srcEndDT.Date orderby b.MatRecvDate descending select b : from b in dbConn.vwMatTrLogs where b.DelStat.Equals(false) & b.MatRecvDate.Value.Date >= srcStartDT.Date & b.MatRecvDate.Value.Date <= srcEndDT.Date & b.MatGrp.Equals(matGroupSrc) orderby b.MatRecvDate descending select b;
                if (getBrand.Count() > 0)
                {
                    GVUMRT.DataSource = getBrand.ToList();
                    GVUMRT.DataBind();
                    lblMsg.Text = "";
                }
                else
                {
                    GVUMRT.DataSource = null;
                    GVUMRT.DataBind();
                    lblMsg.Text = "No data.";
                }
                dbConn.Connection.Close();
            }
            catch (Exception exp)
            {
                lblMsg.Text += exp.Message;
            }
        }

        protected void GVUMRT_Databound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblMatQuantity = (Label)e.Row.Cells[5].FindControl("lblMatQuantity");
                Label lblMatPrice = (Label)e.Row.Cells[6].FindControl("lblMatPrice");
                Label lblMatSum = (Label)e.Row.Cells[7].FindControl("lblMatSum");
                decimal matQuantity = Convert.ToDecimal(lblMatQuantity.Text);
                decimal matPrice = Convert.ToDecimal(lblMatPrice.Text);
                decimal matSum = matQuantity * matPrice;
                lblMatSum.Text = matSum.ToString("#,#.00", CultureInfo.InvariantCulture);
                sumFooterValue += matSum;
                sumQFooterValue += matQuantity;
                foreach (DataControlFieldCell cell in e.Row.Cells)
                {
                    foreach (Control control in cell.Controls)
                    {
                        Button button = control as Button;
                        if (button != null && button.CommandName == "Delete")
                        {
                            button.OnClientClick = "if (!confirm('Are you sure " +
                                   "you want to delete this record?')) return;";
                        }
                    }
                }
            }
            if ((e.Row.RowState & DataControlRowState.Edit) > 0)
            {
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblMatTotal = (Label)e.Row.FindControl("lblMatTotal");
                Label lblMatQuantityTotal = (Label)e.Row.FindControl("lblMatQuantityTotal");
                lblMatTotal.Text = sumFooterValue.ToString("#,#.00", CultureInfo.InvariantCulture);
                lblMatQuantityTotal.Text = sumQFooterValue.ToString("#,#.000", CultureInfo.InvariantCulture);
            }
        }

        protected void GVUMRT_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //this.loadLogsGrid(null, null, null);
            this.loadRefreshGrid();
            GVUMRT.PageIndex = e.NewPageIndex;
            GVUMRT.DataBind();
        }

        protected void GVUMRT_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = GVUMRT.Rows[e.RowIndex];
            Label ID = (Label)row.FindControl("lblID");
            try
            {
                string logsID = ID.Text;
                dbUMRTDataContext dbConn = new dbUMRTDataContext();
                var getData = (from u in dbConn.tbMatTrLogs where u.ID.Equals(logsID) select u).Single();
                getData.DelStat = true;
                dbConn.SubmitChanges();
                dbConn.Connection.Close();

                this.loadRefreshGrid();

            }
            catch (Exception exp)
            {
                lblMsg.Text = exp.Message;
            }
        }

        protected void GVUMRT_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "delData")
            {
                GridViewRow grdRow = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                Label dataID = (Label)GVUMRT.Rows[grdRow.RowIndex].FindControl("lblID");
                hdfDataID.Value = dataID.Text.Trim();
                delPWD.Visible = true;
            }
            else
            {
                hdfDataID.Value = "0";
                delPWD.Visible = false;
            }
            if (e.CommandName == "edtData")
            {
                dbUMRTDataContext dbConn = new dbUMRTDataContext();
                GridViewRow grdRow = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                Label dataID = (Label)GVUMRT.Rows[grdRow.RowIndex].FindControl("lblID");
                string dataIDVal = dataID.Text.Trim();
                var getPData = (from d in dbConn.tbMatTrLogs where d.ID.Equals(dataIDVal) select d).FirstOrDefault();
                hdfDataPID.Value = dataID.Text.Trim();
                tbxPrice.Text = getPData.MatPrice.ToString();
                edtPrice.Visible = true;
                dbConn.Connection.Close();
            }
            else
            {
                hdfDataPID.Value = "0";
                edtPrice.Visible = false;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string startDateSrc = "";
                string endDateSrc = "";
                string matGroup = "";
                string ddStartYearUse = Convert.ToInt32(ddStartYear.SelectedValue.ToString()) < 10 ? "0" + ddStartYear.SelectedValue.ToString() : ddStartYear.SelectedValue.ToString();
                string ddStartMonthUse = Convert.ToInt32(ddStartMonth.SelectedValue.ToString()) < 10 ? ddStartMonth.SelectedValue.ToString() : ddStartMonth.SelectedValue.ToString();
                string ddStartDayUse = Convert.ToInt32(ddStartDay.SelectedValue.ToString()) < 10 ? "0" + ddStartDay.SelectedValue.ToString() : ddStartDay.SelectedValue.ToString();

                string ddEndYearUse = Convert.ToInt32(ddEndYear.SelectedValue.ToString()) < 10 ? "0" + ddEndYear.SelectedValue.ToString() : ddEndYear.SelectedValue.ToString();
                string ddEndMonthUse = Convert.ToInt32(ddEndMonth.SelectedValue.ToString()) < 10 ? ddEndMonth.SelectedValue.ToString() : ddEndMonth.SelectedValue.ToString();
                string ddEndDayUse = Convert.ToInt32(ddEndDay.SelectedValue.ToString()) < 10 ? "0" + ddEndDay.SelectedValue.ToString() : ddEndDay.SelectedValue.ToString();

                startDateSrc = ddStartYearUse + "-" + ddStartMonthUse + "-" + ddStartDayUse;
                endDateSrc = ddEndYearUse + "-" + ddEndMonthUse + "-" + ddEndDayUse;
                matGroup = ddMatGrp.SelectedValue.ToString();
                this.loadLogsGrid(startDateSrc, endDateSrc, matGroup);
            }
            catch (Exception exp)
            {
                lblMsg.Text = "Click Search Error : "+exp.Message;
            }
        }

        protected void loadRefreshGrid()
        {
            try
            {
                string startDateSrc = "";
                string endDateSrc = "";
                string matGroup = "";
                string ddStartYearUse = Convert.ToInt32(ddStartYear.SelectedValue.ToString()) < 10 ? "0" + ddStartYear.SelectedValue.ToString() : ddStartYear.SelectedValue.ToString();
                string ddStartMonthUse = Convert.ToInt32(ddStartMonth.SelectedValue.ToString()) < 10 ? ddStartMonth.SelectedValue.ToString() : ddStartMonth.SelectedValue.ToString();
                string ddStartDayUse = Convert.ToInt32(ddStartDay.SelectedValue.ToString()) < 10 ? "0" + ddStartDay.SelectedValue.ToString() : ddStartDay.SelectedValue.ToString();

                string ddEndYearUse = Convert.ToInt32(ddEndYear.SelectedValue.ToString()) < 10 ? "0" + ddEndYear.SelectedValue.ToString() : ddEndYear.SelectedValue.ToString();
                string ddEndMonthUse = Convert.ToInt32(ddEndMonth.SelectedValue.ToString()) < 10 ? ddEndMonth.SelectedValue.ToString() : ddEndMonth.SelectedValue.ToString();
                string ddEndDayUse = Convert.ToInt32(ddEndDay.SelectedValue.ToString()) < 10 ? "0" + ddEndDay.SelectedValue.ToString() : ddEndDay.SelectedValue.ToString();

                startDateSrc = ddStartYearUse + "-" + ddStartMonthUse + "-" + ddStartDayUse;
                endDateSrc = ddEndYearUse + "-" + ddEndMonthUse + "-" + ddEndDayUse;
                matGroup = ddMatGrp.SelectedValue.ToString();
                this.loadLogsGrid(startDateSrc, endDateSrc, matGroup);
            }
            catch (Exception exp)
            {
                lblMsg.Text = "Click Search Error : " + exp.Message;
            }
        }

        protected void btnDel_Click(object sender, EventArgs e)
        {
            //GridViewRow row = GVUMRT.Rows[e.RowIndex];
            //Label ID = (Label)row.FindControl("lblID");
            string password = tbxPassword.Text;
            if (password == "admin")
            {
                try
                {
                    string logsID = hdfDataID.Value;
                    dbUMRTDataContext dbConn = new dbUMRTDataContext();
                    var getData = (from u in dbConn.tbMatTrLogs where u.ID.Equals(logsID) select u).Single();
                    getData.DelStat = true;
                    dbConn.SubmitChanges();
                    dbConn.Connection.Close();

                    this.loadRefreshGrid();
                    delPWD.Visible = false;

                }
                catch (Exception exp)
                {
                    lblMsg.Text = exp.Message;
                }
            }
            else
            {
                lblMsg.Text = "รหัสผ่านไม่ถูกต้อง";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            hdfDataID.Value = "0";
            delPWD.Visible = false;
            lblMsg.Text = "";
        }

        protected void btnCancelP_Click(object sender, EventArgs e)
        {
            hdfDataPID.Value = "0";
            edtPrice.Visible = false;
            lblMsg.Text = "";
        }

        protected void btnSavePrice_Click(object sender, EventArgs e)
        {
            string pPwd = tbxPasswordP.Text;
            string priceData = tbxPrice.Text;
            if (pPwd == "admin")
            {
                decimal dataUse = (!string.IsNullOrEmpty(priceData)) ? Convert.ToDecimal(priceData) : Convert.ToDecimal("0.00");
                try
                {
                    string logsID = hdfDataPID.Value;
                    dbUMRTDataContext dbConn = new dbUMRTDataContext();
                    var getData = (from u in dbConn.tbMatTrLogs where u.ID.Equals(logsID) select u).Single();
                    getData.MatPrice = dataUse;
                    dbConn.SubmitChanges();
                    dbConn.Connection.Close();

                    this.loadRefreshGrid();
                    delPWD.Visible = false;

                }
                catch (Exception exp)
                {
                    lblMsg.Text = exp.Message;
                }
            }
            else
            {
                lblMsg.Text = "รหัสผ่านไม่ถูกต้อง";
            }
        }
    }
}