﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMRT.Includes.Classes;

namespace UMRT.Templates.Main
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected lazpizFNC objFNC = new lazpizFNC();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.loadMenu();
        }

        protected void loadMenu()
        {
            //string curFile =  Request.CurrentExecutionFilePath.ToString();
            string curFile = this.Page.ToString().Substring(4, this.Page.ToString().Substring(4).Length - 5) + ".aspx";
            //Response.Write(pageName);
            string dataTopMenu = objFNC.retTopMenu(curFile);
            string dataFooterMenu = objFNC.retFooterMenu();
            topMenuDisp.InnerHtml = dataTopMenu;
            footerMenuDisp.InnerHtml = dataFooterMenu;
        }

    }
}