﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Main/Main.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="UMRT.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<div class="art-layout-cell art-sidebar1">
    <div class="art-vmenublock">
	    <div class="art-vmenublock-tl"></div>
	    <div class="art-vmenublock-tr"></div>
	    <div class="art-vmenublock-bl"></div>
	    <div class="art-vmenublock-br"></div>
	    <div class="art-vmenublock-tc"></div>
	    <div class="art-vmenublock-bc"></div>
	    <div class="art-vmenublock-cl"></div>
	    <div class="art-vmenublock-cr"></div>
	    <div class="art-vmenublock-cc"></div>
	    <div class="art-vmenublock-body">
		    <div class="art-vmenublockcontent">
			    <div class="art-vmenublockcontent-body">
				    <ul class="art-vmenu">
					    <li><a href="/" class="active"><span class="l"> </span><span class="r"> </span><span class="t">หน้าหลัก</span></a></li>
					    <li><a href="/"><span class="l"> </span><span class="r"> </span><span class="t">รายการที่กำลังสั่งซื้อ</span></a></li>
                        <li><a href="/"><span class="l"> </span><span class="r"> </span><span class="t">รายการที่สั่งซื้อไปแล้ว</span></a></li>
                        <li><a href="/"><span class="l"> </span><span class="r"> </span><span class="t">ค้นหารายชื่อสารเคมี</span></a></li>
                        <li><a href="/"><span class="l"> </span><span class="r"> </span><span class="t">ค้นหารายชื่อสารเคมี จาก C-Number</span></a></li>
                        <li><a href="/"><span class="l"> </span><span class="r"> </span><span class="t">เปลี่ยนแปลง Password</span></a></li>
				    </ul>
				    <div class="cleared"></div>
			    </div>
		    </div>
		    <div class="cleared"></div>
	    </div>
    </div>
    <div class="art-block">
	    <div class="art-block-body">
		    <div class="art-blockheader">
			    <h3 class="t">Lab Chemical and Supply Management (LCSP)</h3>
		    </div>
		    <div class="art-blockcontent">
			    <div class="art-blockcontent-body">
				    <p>Enter Block content here...</p>
				    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam pharetra, tellus sit amet congue vulputate, nisi erat iaculis nibh, vitae feugiat sapien ante eget mauris. </p>
				    <div class="cleared"></div>
			    </div>
		    </div>
		    <div class="cleared"></div>
	    </div>
    </div>
    <div class="cleared"></div>
    </div>--%>
    <div class="art-layout-cell art-content">
    <div class="art-post">
	    <div class="art-post-body">
		    <div class="art-post-inner art-article">
			    <h2 class="art-postheader">
                    UTILITY MATRIAL RECEIVE TRACKING (UMRT) 
                </h2>
			    <div class="art-postcontent">
                    <div style="margin-right: auto; margin-left: auto; width: 90%;text-align:center;padding:50px;">
                        <div style="padding:10px;">
                            <fieldset>
                                <legend>Search Parameter</legend>
                                <label>FROM : </label>
                                <asp:DropDownList ID="ddStartDay" runat="server">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddStartMonth" runat="server">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddStartYear" runat="server">
                                </asp:DropDownList>
                                &nbsp;
                                <label>TO : </label>
                                <asp:DropDownList ID="ddEndDay" runat="server">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddEndMonth" runat="server">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddEndYear" runat="server">
                                </asp:DropDownList>
                                &nbsp;
                                <label>Matrial Group : </label>
                                <asp:DropDownList ID="ddMatGrp" runat="server">
                                </asp:DropDownList>
                                <%--&nbsp;
                                <label>Matrial Name : </label>
                                <asp:DropDownList ID="ddMatName" runat="server">
                                </asp:DropDownList>--%>&nbsp;
                                <asp:Button ID="btnSearch" runat="server" Text="แสดง" 
                                    onclick="btnSearch_Click" />
                            </fieldset>
                        </div>
                        <div ID="delPWD" runat="server" visible="false">
                            กรุณาใส่ Password เพื่อลบรายการ
                            <asp:TextBox ID="tbxPassword" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:Button ID="btnDel" runat="server" Text="ลบ" Width="50px" 
                                onclick="btnDel_Click" />
                            <asp:Button ID="btnCancel" runat="server" Text="ยกเลิก" 
                                onclick="btnCancel_Click" />
                            <asp:HiddenField ID="hdfDataID" runat="server" Value="0" />
                        </div>
                        <div ID="edtPrice" runat="server" visible="false">
                            ใส่รหัสเพื่อแก้ไขราคา
                            <asp:TextBox ID="tbxPasswordP" runat="server" TextMode="Password"></asp:TextBox>
                            แก้ไขราคา
                            <asp:TextBox ID="tbxPrice" runat="server"></asp:TextBox>
                            <asp:Button ID="btnSavePrice" runat="server" Text="บันทึก" Width="50px" 
                                onclick="btnSavePrice_Click" />
                            <asp:Button ID="btnCancelP" runat="server" Text="ยกเลิก" 
                                onclick="btnCancelP_Click" />
                            <asp:HiddenField ID="hdfDataPID" runat="server" Value="0" />
                        </div>
                        <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="Red" Font-Bold="True"></asp:Label>
                        <asp:GridView ID="GVUMRT" runat="server"
                            Width="100%"  
                            AllowSorting="True" AutoGenerateColumns="False" 
                            OnRowDataBound = "GVUMRT_Databound"
                            OnPageIndexChanging="GVUMRT_OnPageIndexChanging" 
                            OnRowCommand="GVUMRT_RowCommand"
                            ShowFooter="True"
                            >
                            <Columns>
                                <asp:TemplateField HeaderText="ID" SortExpression="id">
                                    <ItemTemplate>
                                        <asp:Label ID="lblID" runat="server" Text='<%#Bind("ID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Receive Date" SortExpression="MatRecvDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMatRecvDate" runat="server" Text='<%#Bind("MatRecvDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Matrial Group" SortExpression="MatGrpTxt">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMatGrpTxt" runat="server" Text='<%#Bind("MatGrpTxt") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Matrial Name" SortExpression="MatGrpTxt">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMatData" runat="server" Text='<%#Bind("matData") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Receive By" SortExpression="RecvBy">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRecvBy" runat="server" Text='<%#Bind("RecvBy") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="xxx" runat="server" Text="Total"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Quantity" SortExpression="MatQuantity" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblMatQuantity" runat="server" Text='<%# Eval("MatQuantity", "{0:n3}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblMatQuantityTotal" runat="server" Text="Label"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Price" SortExpression="MatPrice">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMatPrice" runat="server" Text='<%# Eval("MatPrice", "{0:n3}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sum" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMatSum" runat="server" Text=''></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblMatTotal" runat="server" Text="Label"></asp:Label>
                                    </FooterTemplate>
                                    <ItemStyle HorizontalAlign="Right" /> 
                                </asp:TemplateField>
                                <asp:TemplateField >
                                    <ItemTemplate>
                                        <asp:Button ID="btnEdt" runat="server" CommandName="edtData" Text="แก้ไขข้อมูล" BackColor="#00ff48" />
                                        <asp:Button ID="btnDel" runat="server" CommandName="delData" Text="ลบข้อมูล" BackColor="#FFC9CE" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#99B4E8" Font-Bold="True" ForeColor="#333333" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <HeaderStyle BackColor="#507BD1" Font-Bold="True" ForeColor="White" />
                            <EditRowStyle BackColor="#2461BF" />
                            <AlternatingRowStyle BackColor="White" />      
                        </asp:GridView>
                    </div>
			    </div>
			    <div class="cleared"></div>
		    </div>
		    <div class="cleared"></div>
	    </div>
    </div>
    <div class="cleared"></div>
</div>
</asp:Content>
