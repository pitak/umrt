﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;

namespace UMRT.WS
{
    public partial class CreateUpdData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.createUpdateFile();
        }

        private void createUpdateFile()
        {
            this.GetMatrialGrp();
            this.GetMatrialData();
            this.GetVendorData();
        }


        private void GetMatrialGrp()
        {
            dbUMRTDataContext dbConn = new dbUMRTDataContext();
            try
            {
                var serializer = new JavaScriptSerializer();
                var Data = from g in dbConn.tbMatGrps
                           where g.Enable.Equals(true)
                           select new
                           {
                               mgID = g.ID,
                               mgData = g.matGrp
                           };
                string json = serializer.Serialize(Data);
                // Write the string to a file.
                System.IO.StreamWriter file = new System.IO.StreamWriter(Server.MapPath("~") + "/StoreData/public_matrial_group.txt");
                file.WriteLine(json);
                file.Close();
                dbConn.Connection.Close();
            }
            catch (Exception exp)
            {
            }
        }

        private void GetMatrialData()
        {
            dbUMRTDataContext dbConn = new dbUMRTDataContext();
            try
            {
                var serializer = new JavaScriptSerializer();
                var Data = from d in dbConn.vwMatrials
                           where d.Enable.Equals(true)
                           select new
                           {
                               mID = d.ID,
                               mGrp = d.matGrp,
                               mGrpName = d.mGroup,
                               mData = d.matData,
                               mUnit = d.matUnit,
                               mUnitName = d.mUnit
                           };
                string json = serializer.Serialize(Data);
                // Write the string to a file.
                System.IO.StreamWriter file = new System.IO.StreamWriter(Server.MapPath("~") + "/StoreData/public_matrial_data.txt");
                file.WriteLine(json);
                file.Close();
                dbConn.Connection.Close();
            }
            catch (Exception exp)
            {
            }
        }

        private void GetVendorData()
        {
            dbUMRTDataContext dbConn = new dbUMRTDataContext();
            try
            {
                var serializer = new JavaScriptSerializer();
                var Data = from v in dbConn.tbVendors
                           where v.Enable.Equals(true)
                           select new
                           {
                               vID = v.ID,
                               vName = v.Vendor
                           };
                string json = serializer.Serialize(Data);
                // Write the string to a file.
                System.IO.StreamWriter file = new System.IO.StreamWriter(Server.MapPath("~") + "/StoreData/public_vendor_data.txt");
                file.WriteLine(json);
                file.Close();
                dbConn.Connection.Close();
            }
            catch (Exception exp)
            {
            }
        }
    }
}