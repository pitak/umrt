﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Globalization;
using Newtonsoft.Json;

namespace UMRT.WS
{
    public partial class SaveData : System.Web.UI.Page
    {
        public class matLogDataJson
        {
            public int matGrp { get; set; }
            public int matItem { get; set; }
            public decimal matAmount { get; set; }
            public decimal matPrice { get; set; }
            public int matVendorSlc { get; set; }
            public string matVendor { get; set; }
            public string matPO { get; set; }
            public string matRecvDate { get; set; }
            public string matRecvTime { get; set; }
            public string matRecvUsr { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Dictionary<string, string> retDic = new Dictionary<string, string>();
            //if (true)
            if ((Request.Form != null) && (Request.Form.Count > 0))
            {
                
                try
                {
                    string dataLogsSend = Request.Form[0];
                    //string dataLogsSend = "{'matGrp':'1','matItem':'3','matAmount':'123.4','matPrice':'456.7','matVendorSlc':'0','matVendor':'KTP','matPO':'677888','matRecvDate':'6/28/2017','matRecvTime':'10:24:58.3272940','matRecvUsr':'Chai'}";
                    dbUMRTDataContext dbConn = new dbUMRTDataContext();
                    int mGrpID, mItemID;
                    DateTime dt;
                    string dataSendUse = dataLogsSend.Replace("\\", "");
                    var jObjData = new JavaScriptSerializer().Deserialize<matLogDataJson>(dataSendUse);
                    string[] timeArr = jObjData.matRecvTime.Split(':');
                    string recvDateStr = jObjData.matRecvDate + " " + timeArr[0] + ":" + timeArr[1];
                    //Response.Write(recvDateStr+"<br/>");
                    DateTime dateLogs = DateTime.ParseExact(recvDateStr, "M/d/yyyy HH:mm", CultureInfo.InvariantCulture);
                    mGrpID = Convert.ToInt32(jObjData.matGrp);
                    mItemID = Convert.ToInt32(jObjData.matItem);
                    dt = dateLogs;

                    //var x = from dd in dbConn.tbMatTrLogs where dd.MatGrp.Equals(mGrpID) & dd.MatItem.Equals(mItemID) & dd.MatRecvDate.Value.Day.Equals(dt.Day) & dd.MatRecvDate.Value.Month.Equals(dt.Month) & dd.MatRecvDate.Value.Year.Equals(dt.Year) select dd;
                    //foreach (var xd in x)
                    //{
                    //    dbConn.tbMatTrLogs.DeleteOnSubmit(xd);
                    //}
                    //dbConn.SubmitChanges();
                    var chkExistData = (from m in dbConn.tbMatTrLogs where m.MatGrp.Equals(mGrpID) & m.MatItem.Equals(mItemID) & m.MatRecvDate.Value.Day.Equals(dt.Day) & m.MatRecvDate.Value.Month.Equals(dt.Month) & m.MatRecvDate.Value.Year.Equals(dt.Year) select m).Count();
                    if (true)
                    {
                        tbMatTrLog m = new tbMatTrLog();
                        m.MatGrp = (Int32)jObjData.matGrp;
                        m.MatItem = (Int32)jObjData.matItem;
                        m.MatQuantity = (decimal)jObjData.matAmount;
                        m.MatPrice = (decimal)jObjData.matPrice;
                        m.MatVendorSlc = (Int32)jObjData.matVendorSlc;
                        m.MatVendor = jObjData.matVendor.ToString();
                        m.MatPO = jObjData.matPO.ToString();
                        m.MatRecvDate = dateLogs;
                        m.RecvBy = jObjData.matRecvUsr.ToString();
                        m.DateLogs = DateTime.Now;
                        m.DelStat = false;
                        dbConn.tbMatTrLogs.InsertOnSubmit(m);
                        dbConn.SubmitChanges();
                    }
                    dbConn.Connection.Close();
                    //Response.Write("บันทึกข้อมูลลงฐานข้อมูล เรียบร้อย !");
                    retDic.Add("retVal", "true");
                    retDic.Add("retMsg", "บันทึกข้อมูลลงฐานข้อมูล เรียบร้อย !");
                    string retDicJson = JsonConvert.SerializeObject(retDic);
                    Response.Write(retDicJson);
                }
                catch (Exception exp)
                {
                    //Response.Write("เกิดความผิดพลาดในการส่งข้อมูลไปยัง Server กรุณาลองใหม่อีกครั้ง." + exp.Message);
                    retDic.Add("retVal", "false");
                    retDic.Add("retMsg", "เกิดความผิดพลาดในการส่งข้อมูลไปยัง Server กรุณาลองใหม่อีกครั้ง." + exp.Message);
                    string retDicJson = JsonConvert.SerializeObject(retDic);
                    Response.Write(retDicJson);
                }
            }
            else
            {
                retDic.Add("retVal", "false");
                retDic.Add("retMsg", "ไม่มีข้อมูลส่งมาที่ Server");
                string retDicJson = JsonConvert.SerializeObject(retDic);
                Response.Write(retDicJson);
            }
        }
    }
}